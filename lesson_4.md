# Lesson Four - Add Some Scripting to Your CGI Scripting

## Putting it all together

Notice this bit '?user=sven&age=20' in your URL

'http://192.168.7.214/cgi-bin/formsubmit.cgi?user=sven&age=20'

Fine in this context but you wouldn't want that there for a form where
your users were submitting credit card numbers.

We're going to fix this by changing the method from GET to POST.

Which means we're going to have to get rid of this

```
if [ "$REQUEST_METHOD" != "GET" ]; then
```

Which actually opens up a new possibility. Instead of having two pages
wouldn't it be nicer if all our code was in one place?



But first let's introduce wireshark and look at the requests on the wire.
because this really isn't about learning CGI. It's about learning how
web applications interact on the wire. We're learning the HTTP protocol.

And how the front end client, which in this case is more than just the browser
but the application you write which runs in the browser, interacts with
the back end.

We care mostly about this information.


REQUEST_URI=/cgi-bin/env.sh?namex=valuex&namey=valuey&namez=valuez
HTTP_CONNECTION=keep-alive
HTTP_ACCEPT=text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5
QUERY_STRING=namex=valuex&namey=valuey&namez=valuez
REQUEST_METHOD=GET




```
#!/bin/bash

echo "Content-type: text/html"
echo ""

echo '<html>'
echo '<head>'
echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">'


if [ "$REQUEST_METHOD" == "GET" ]; then
  echo '<title>Hello There</title>'
  echo '</head>'
  echo '<body>'
  echo '<form method=POST action="/cgi-bin/formcomplete.cgi">'
  echo '<p> What is your name? </p>'
  echo '<p><input type="text" name="user" size=12></p>'
  echo '<p> What is your age? </p>'
  echo '<p><input type="text" name="age" size=3></p>'
  echo '<p><input type="submit" value="Submit"></p>'
  echo '</form>'
else
  # Parse the query string for username and age
  QUERY=$(echo ${QUERY_STRING} | cut -f2- -d'=')
  USERNAME=$(echo ${QUERY} | cut -f1 -d'&')
  USERAGE=$(echo ${QUERY} | cut -f2 -d'&')

  # Build the response
  printf "<title>Hello %s</title>\n" "${USERNAME}"
  echo '</head>'
  echo '<body>'

  echo "<p>"
  printf "Hello %s\n" "${USERNAME}"
  echo "</p>"
  echo "<p>"
  echo "You"
  if [[ ${USERAGE} -gt 30 ]]
  then
    echo " old fogie "
  else
    echo " young buck "
  fi
  echo "you!"
  echo "</p>"
fi

echo '</body>'
echo '</html>'
```

Now look at your URL `http://192.168.7.214/cgi-bin/formcomplete.cgi`

## Splitting it Back Apart. But for a Reason This Time - Introducing Functions

Create functions for header and footer

Then create functions for the bodies and turn the if/else into a switch (If Bash supports that) for the requested routes


