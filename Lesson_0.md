# Lesson Zero - Workstation Set Up

Workstation setup for windows users.

If you are on a MacOS or Linux the neccessary software should already be installed.

Must have Windows 7 or higher

# Install Some Softwares

## Install Git

1. Download the 64 bit "Git for Windows Setup" from here https://git-scm.com/download/win
2. For Install type choose full.
3. For "Do you want to install Git bash" say yes.
4. For "Adjusting your PATH environment." And the option is "Use git bash only."
5. For "hich terminal emulator to use with Git Bash" choose MinTTY
6. For "Configuring extra options"
   * "Enable file system caching" - no
   * "Enable git credential manager" - Yes
   * "Enable symbolic links' - No


## Install Slack
If you know me, let me know and I'll send you an invitation to a slack workspace called "TheUberlab"
where you can ask me questions.

1. Download slack from here https://slack.com/downloads/windows
2. Once installed it will show you what looks like a login window. But this
will actually create a new workspace. Go back to the email from slack inviting
you to TheUberlab and follow the directions there.


## Install Visual Studio Code
This won't be needed until later lessons but might as well install it now.

NOTE: This is actually different than "Visual Studio."

Get it from here
https://code.visualstudio.com/download


## Install WSLTTY

Download and install wsltty from here: https://github.com/mintty/wsltty
It is a much better shell.

## Configure WSLTTY and/or Git Bash to be not so horrible
1. Right click on the app icon in the upper left corner
1. Choose "options"
1. Select "Keys"
1. Under "Shortcuts" ensure "Copy and Paste (Ctrl/Shift+ins) is selected
1. Select "Selection"
1. Deselect "Copy on select", "Copy as rich text" and "Trim space from selection". Hell deselect everything.

NOTE: You may want to change the default window size under "Window"

## Enable Windows Subsystem for Linux
1. Click the start menu
1. Click Settings
1. Select "Apps"
1. In the upper right under "Related Settings" click the link for "Programs and Features"
1. On the left click the link for "Turn Windows features on or off"
1. Scroll down until you find "Windows Subsystem for Linux" and make sure it's enabled
1. Click "OK"

## Install Ubuntu
1. Open the Microsoft Store
1. Search for "Ubuntu"
1. Select "Ubuntu 20.xx LTS"
1. Click "Get" and/or "Install"

## Configure File Permissions for Ubuntu
1. Open wsltty and vi /etc/wsl.conf. The file will probably not exist.
1. Add the following Settings

```
# Enable extra metadata options by default
[automount]
enabled = true
root = /mnt/
options = "metadata,umask=22,fmask=11"
mountFsTab = true
```

1. Open the windows terminal (`cmd` in the windows search box)
1. Run `wsl --shutdown` to stop any wsl instances
1. Open wsl (via wsltty) and confirm that you can see file permissions now.
