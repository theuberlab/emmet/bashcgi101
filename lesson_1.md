# Chapter One - Getting Started

## What is the Shell
In it's simplest form your shell is the command line interface to your operating
system. It allows you to navigate your filesystem and run other programs.

However shells are more than that. They are also full programming languages.

The first widely used shell was created for Unix in the late 70s and is still
in use today. It's called the "Bourne Shell" after it's author Stephen Bourne
and it's executable is simply `sh`.

Through the 80s other shells were introduced such as the "C shell" `csh` which
had a more c (programming language) like syntax and was much more powerful
for programming. In the late eighties Brian Fox began work on a new shell
which would be capable of running Bourne Shell scripts but also have more
advanced programming functionality like csh but using a syntax more
familiar to users of Bourne. This shell was named the "Bourne Again Shell"
but is more commonly known by it's executable name `bash`. Bash also introduced
command line completion and performance improvements.

Since then Bash has become the default shell for most Linux distributions
and today is still the most popular shell on the internet.

## Basic HTML Page
You should already have a basic understanding but
let's make a static html page again.

### Start a web server

If on fedora you'll need to open port 80.  
`firewall-cmd --add-port=80/tcp --permanent`


### Edit or Create index.html
In the web server's document root

```
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Hello World</title>
</head>
<body>
Hello World
</body>
</html>
```

### Access the Site From Your Browser.

### Access the Site With Curl
We will use curl to do most of our debugging.


### More Complex HTML

TODO: Add a second page and show how the browser renders them both but
curl does not.
