# Lesson Six - Working With a Back-End

## Reading and Writing to Databases

Introduce the issue of mutiple access.

Update the dice app to allow you to store named sets of dice+modifiers

Which means we need some form of authn/z (REAL basic) and some way to
track session information.

Spin up some stand alone in memory DB as the back end.

Configure load balancing and see how that screws up in memory DBs
This is why one of the cloud mantras is cattle not pets.

Move the DB into AWS RDS and see how the problem goes away

