# Lesson three - Intro to CGI Scripting

## Overview of CGI
CGI or ["Common Gateway Interface"](https://en.wikipedia.org/wiki/Common_Gateway_Interface)  
is a mechanism for a web server to pass user requests to an external
program, such as a bash script, which then generates an html response
and returns it to the requestor. I.E. the web server is acting as a gateway
to the external program.

CGI is actually an older mechanism for generating dynamic content but it
allows us to use a very simple programming language, like bash, to learn
a few key concepts about dynamic web content and networking in general.

## Basic CGI Scripting

Let's think back to our first lesson where we created a static web page.
Now let's combine it with the first excersize of lesson two where we learned
how to make a program print text.

Remember this HTML?
```
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Hello World</title>
</head>
<body>
Hello World
</body>
</html>
```

Let's make a script that prints that out.

`sudo vi /var/www/cgi-bin/hello.cgi` and make it look like the following.

```
#!/bin/bash

# We will explain why these two lines are separate shortly
echo "Content-type: text/html"
echo ""

# This bit should look pretty familiar
echo '<html>'
echo '<head>'
echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">'
echo '<title>Hello World</title>'
echo '</head>'
echo '<body>'
echo 'Hello World'
echo '</body>'
echo '</html>'
```

Remember to make it executable `chmod +x /var/www/cgi-bin/hello.cgi`.

One of the great things about starting with CGI is that we can
test the output of our code without getting the web server involved.

If we run `/var/www/cgi-bin/hello.cgi` it should look something like this.

```
[sporkboy@localhost scripting101]$ /var/www/cgi-bin/hello.cgi
Content-type: text/html

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Hello World</title>
</head>
<body>
Hello World
</body>
</html>
[sporkboy@localhost scripting101]$ 
```

Now open your browser and enter `http://<your servers ip address>/cgi-bin/hello.cgi

Contrats!

Now remember when we said your scripts wouldn't work without specifying the
interpreter? Let's try that. Modify hello.cgi and remove the first line
`#!/bin/bash`

Try your web page again.

you should see something like:


*Internal Server Error*

The server encountered an internal error or misconfiguration and was unable to complete your request.

Please contact the server administrator at root@localhost to inform them of the time this error occurred, and the actions you performed just before this error.

More information about this error may be available in the server error log.



## CGI Environment Variables

Add some notes about environment variables.

Throw this together to see all of them. Most of which we don't care about.
`sudo vi /usr/lib/cgi-bin/helloenv.cgi`

```
#!/bin/bash

# We will explain why these two lines are separate shortly
echo "Content-type: text/html"
echo ""


echo '<html>'
echo '<head>'
echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">'
echo '<title>Environment Variables</title>'
echo '</head>'
echo '<body>'
echo '<h2>Environment Variables:</h2>'
echo '<pre>'
/usr/bin/env
echo '</pre>'

echo '</body>'
echo '</html>'
```


## Procesing Forms

Do like this `sudo vi /var/www/html/form.html /usr/lib/cgi-bin/formsubmit.cgi`

```
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Form Example</title>
    </head>
    <body>
        <form method=GET action="/cgi-bin/formsubmit.cgi">
        <p> What is your name? </p>
        <p><input type="text" name="user" size=12></p>
        <p> What is your age? </p>
        <p><input type="text" name="age" size=3></p>
        <p><input type="submit" value="Submit"></p>
        </form>
    </body>
</html>
```

```
#!/bin/bash

# Parse the query string for username and age
USERNAME=$(echo ${QUERY_STRING} | cut -f1 -d'&' | cut -f2 -d'=')
USERAGE=$(echo ${QUERY_STRING} | cut -f1 -d'&' | cut -f2 -d'=')


# Build the response
echo "Content-type: text/html"
echo ""

echo '<html>'
echo '<head>'
echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">'
printf "<title>Hello %s</title>\n" "${USERNAME}"
echo '</head>'
echo '<body>'


  # Make sure we have been invoked properly.

  if [ "$REQUEST_METHOD" != "GET" ]; then
        echo "<hr>Script Error:"\
             "<br>Usage error, cannot complete request, REQUEST_METHOD!=GET."\
             "<br>Check your FORM declaration and be sure to use METHOD=\"GET\".<hr>"
        exit 1
  else
    echo "<p>"
    printf "Hello %s\n" "${USERNAME}"
    echo "</p>"

    echo "<p>"
    echo "You"
    if [[ ${USERAGE} -gt 30 ]]
    then
      echo " old fogie "
    else
      echo " young buck "
    fi
    echo "you!"
    echo "</p>"
  fi

echo '</body>'
echo '</html>'
```
