# Lesson Two - Intro to Bash Scripting

## Basic Shell Scripting
In it's simplest form a bash script is just a series of commands.
Any command you can run on the command line you can run in a script.

Run the following command
`echo "hello world"`

Your output should be something like this:

```
$ echo "hello world"
hello world
```

Now let's turn it into a script. Do like this: `vi helloworld.bash`
add a single line `echo "hello world"` to this file then save and quit.

Now run bash passing your script as an argument like so:

```
$ bash helloworld.bash
hello world
```

And there it is your first script!

## Variables
let's make it a bit more interesting and introduce variables.
The code you've written so far is called "static" The results will never
change because the code does not allow for variation.

Introducing variables.

### Variable Types
The simplest variable is called a scalar. In Bash scalars are represented
as nearly any string of ASCII characters and numbers. However it's best to  
limit them to letters and numbers since many special characters tell bash  
to do something else.

Scalar variables can hold any simple data such as a "rune" or letter 'c' a string
(which is actually a list of runes) such as "cat" or "My mom and your mom
were hanging out clothes." or numbers like 1 or 20515306124.

We "set" or "assign" a value to a variable by adding the word, a single  
equals sign, then the content you wish to assign to the variable.

```
stringvar="my string"
integervar=132
```

We "access" the contents of the variable by using the variable name, prepended  
with a '$' dollar sign.

```
echo $stringvar
echo $integervar
```

Let's try that now. Edit your code and change it to the following.

```
myvar="Hello World"

echo $myvar

myvar="That is all"

echo $myvar
```

Your output should look like this

```
$ bash helloworld.bash
Hello World
That is all
```

Do you see how even though the echo statement is identical `echo $myvar`
The contents of the variable `$myvar` are different each time we call echo.


## Simplifying Execution
It's early so you're probably not sick of typing `bash ` in front of your
script name every time you run it. But you would get sick of it quickly and
begin to wish that you could just "run your programs." Good news is, with
two minor changes, you can.

The first has to do with permissions. We can attempt to execute our script
by simply typing the path to it on the command line. Since your current working
directory is likely NOT in your $PATH (more on this later) you will have
to explicitely provide the path to the file. This is easiest done by using
the '.' reference to CWD `./helloworld.bash`

Do this now and you should see the following:
```
$ ./helloworld.bash
-bash: ./helloworld.bash: Permission denied
```

Let's take a look at the permissions with `ls -l`

```
$ ls -l helloworld.bash
-rw-r--r--  1 sporkboy  developers  67 Dec 30 16:20 helloworld.bash
```

We can see from the permissions block on the left that the owner of the file
("sporkboy") has read 'r' and write 'w' permissions. The group ("developers")
has only read and everyone else also has read.

Let's add executable permissions (for all 3 classes of user) using the "Change
Mode" command `chmod`.

```
$ chmod +x helloworld.bash 
$ ls -l helloworld.bash
-rwxr-xr-x  1 sporkboy  developers  67 Dec 30 16:20 helloworld.bash*
```

Notice the new "executable" 'x' setting in the permissions section has
been applied to all three classes of user.

### Shebang
There is a second change we will need to make for this to work properly.
If you try and run this right now the same way, `.helloworld.bash`, it will
work. But not for the reason you think. You're still not simply "running
your program." Remember that the shell is also the command line interface
to your OS. So the command prompt you are running this script from IS
actually bash. When we move on to CGI it will be your web server calling
your script and your web server won't know how to interpret the code.

You solve this by defining the interpreter as the first line of your script.

Add the following to your script as the first line.
`#!/bin/bash` That's pound bang bin bash. Other scripting languages use
the same syntax but specify a different inerpreter.

Some others you might see:
Python `#!/bin/python3`
Korn shell `#!/bin/ksh`
Ruby `#!/bin/ruby`



### Assigning Command Output to Variables
We've said anything you can execute on the command line you can execute
in a script. But often we want to take the output of a command and use it
later. We can do this by assigning the results of the command to a variable.

Let's create a new script called 'whereAndWhen'. 'whereAndWhen' will
tell us the time and what computer we are logged into.

To get the time we will use the `date` command.

```
[sporkboy@localhost scripting101]$ date
Wed Dec 30 08:02:25 PM EST 2020
[sporkboy@localhost scripting101]$ 
```

To view the hostname we use the `hostname` command.

```
[sporkboy@localhost scripting101]$ hostname
ragnarok
[sporkboy@localhost scripting101]$ 
```

Let's put those into a script. But first let's learn about comments.

As scripts and programs get larger the need for documentation becomes greater.
It's a good practice to keep your documentation as close to the thing
you are documenting as possible. Not only does it make finding the documentation
easier but if you have to jump through a lot of hoops to update it your
documentation will quickly become out of date. And it's hard to get any  
closer to your script than within the actual script.

Most programming languages support some form of "comments". These are blocks
of code which are used to document the inner workings or usage of your code.  
And which the interpreter (or compiler) ignores. In most scripting languages
a comment is any string of text starting with a pound sign '#' up until
the first newline it encounters.

Like:

`# This line is ignored by bash`

Now let's put all this into practice.

`vi whereAndWhen` and add the following:

```
#!/bin/bash
 
# There are two ways to run an external program and return it's results to a variable.
# The most reliable is with `$()`
now="$(date)"

# But you will frequently see grave accents or "back ticks" used
computer_name=`hostname`
 
#
# Remember that variable names are case sensitive $now and $NOW are different names
#

echo "Current date and time : $now"
# Another way of printing
printf "Computer name : %s\n" "$computer_name"
echo ""
```

Don't forget to make your script executable or call it with bash.

```
[sporkboy@localhost scripting101]$ chmod +x whereAndWhen 
[sporkboy@localhost scripting101]$ ./whereAndWhen 
Current date and time : Wed Dec 30 08:08:18 PM EST 2020
Computer name : ragnarok

[sporkboy@localhost scripting101]$ 
```

## Logic Control
If you've ever seen someone elses code it probably looks a lot more  
complex than the examples so far. Once you are trying to solve real problems
with your code you will find it nearly impossible without some more advanced
logic.

We'll cover some in subsequent chapters but some examples of logic are:

### Conditional Statements
Conditional statements are how you make decisions. The most common is
the `if` statement. I.E. "If this do that"

```
#!/bin/bash
directory="./someDirectory"

# bash check if directory exists
if [ -d $directory ]; then
        echo "Directory exists"
fi
```

### Looping
Often we need to iterate over a number of items and do the same thing to
each one.

This is done with a loop. The simplest loop is the "for loop".

```
#!/bin/bash

# bash for loop
for f in $( ls /var/ ); do
	echo $f
done 
```

### Conditional Looping
Perhaps you need to just loop as long as a certain condition is met ("While")  
such as checking that something is healthy. Or "until" some condition is  
met.

The following example will echo the value of `$COUNT` until it reaches zero.

```
#!/bin/bash
COUNT=6
# bash while loop
while [ $COUNT -gt 0 ]; do
	echo Value of count is: $COUNT
	let COUNT=COUNT-1
done 
```

Feel free to experiment with these in your own code.
For more detail on scripting in bash take a peek at some popular
tutorials available on the web such as https://linuxconfig.org/bash-scripting-tutorial
