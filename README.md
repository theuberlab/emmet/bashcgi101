# Bash CGI 101

An introduction to dynamic web design using CGI and bash

## Who this is for

These tutorials are intended to help introduce you to users new to the field  
of computer science. Engineering, programming, systems administration.

It uses HTML, the Bash shell and CGI to create some small web pages intended
to teach some of the fundamentals of HTTP and web application communiations.

Basic familiarity with a unix terminal and vi is assumed.

## Environment set up.

Get started by configuring the software you will need to go through this tutorial [in Lesson 0 here](Lesson_0.md)

These tutorials are intended to go hand in hand with learning AWS.

The steps for creating and configuring your AWS instances are in [a companion
tutorial here](https://gitlab.com/theuberlab/emmet/aws)
